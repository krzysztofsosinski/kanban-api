package pl.ks.user;

import org.assertj.core.util.DateUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import pl.ks.user.domain.token.impl.JwtTokenServiceImpl;
import pl.ks.user.domain.token.impl.TimeProviderImpl;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

public class JwtTokenServiceTest {

    private static final String TEST_USERNAME = "test-user";

    @Mock
    private TimeProviderImpl timeProviderImpl;

    @InjectMocks
    private JwtTokenServiceImpl tokenManager;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        ReflectionTestUtils.setField(tokenManager, "EXPIRES_IN", 10); // 10 sec
        ReflectionTestUtils.setField(tokenManager, "SECRET", "mySecret");
        ReflectionTestUtils.setField(tokenManager, "APP_NAME", "Kanban");
    }

    @Test
    public void testGenerateTokenGeneratesDifferentTokensForDifferentCreationDates() {
        when(timeProviderImpl.now())
                .thenReturn(DateUtil.yesterday())
                .thenReturn(DateUtil.now());

        final String token = createToken();
        final String laterToken = createToken();

        assertThat(token).isNotEqualTo(laterToken);
    }

    private String createToken() {
        return tokenManager.generateToken(TEST_USERNAME);
    }

}
