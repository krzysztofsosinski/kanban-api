package pl.ks.common;

import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.ks.user.domain.user.User;

import java.util.Optional;

public class AuditorAwareImpl extends AbstractAuditable implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
    }
}
