package pl.ks.board.domain.board;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ks.board.boundry.model.kanban.TaskDto;
import pl.ks.board.infrastructure.exception.ResourceNotFoundException;
import pl.ks.board.infrastructure.util.ServiceUtil;

import javax.transaction.Transactional;
import java.util.Map;
import java.util.Optional;

@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final CardRepository cardRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository, CardRepository cardRepository) {
        this.taskRepository = taskRepository;
        this.cardRepository = cardRepository;
    }

    @Transactional
    public Task patchTask(Long taskId, Map<String, Object> fields) {
        return taskRepository.findById(taskId)
                .map(task -> taskRepository.save(ServiceUtil.patchObject(fields, task, Task.class)))
                .orElse(null);
    }

    @Transactional
    public Task createTask(Long cardId, TaskDto taskDTO) {
        Optional<Card> card = cardRepository.findById(cardId);
        if (card.isPresent()) {
            Task task = taskDTO.asTask();
            task.setId(null);
            task.setCard(card.get());
            return taskRepository.save(task);
        }

        throw new ResourceNotFoundException("Card with " + cardId + "does not exist");
    }

    @Transactional
    public void deleteTask(Long taskId) {
        taskRepository.deleteById(taskId);
    }
}
