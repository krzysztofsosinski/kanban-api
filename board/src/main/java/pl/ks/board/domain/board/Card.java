package pl.ks.board.domain.board;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CARDS")
public class Card implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "card_id_seq")
    @SequenceGenerator(name = "card_id_seq", sequenceName = "card_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;

    @Basic
    @Column(length = 512, nullable = false)
    private String title;

    @Basic
    @Column(columnDefinition = "text")
    private String description;

    @Basic
    @Column(length = 10)
    private String color;

    @Basic
    private String status;

    @Basic
    @Column(name = "create_at")
    private Date createAt;

    @Basic
    @Column(name = "updated_at")
    private Date updatedAt;

    @Basic
    @Column(name = "row_order")
    private Integer rowOrder;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "card_id")
    private List<Task> tasks = new ArrayList<>();

    protected Card() {
    }

    public Card(Long id, String title, String description, String color,
                String status, Date createAt, Date updatedAt, Integer rowOrder) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.color = color;
        this.status = status;
        this.createAt = createAt == null ? new Date() : createAt;
        this.updatedAt = updatedAt;
        this.rowOrder = rowOrder;
    }

    public void addTask(Task task) {
        tasks.add(task);
        task.setCard(this);
    }

    public void removeTask(Task task) {
        tasks.remove(task);
        task.setCard(null);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getRowOrder() {
        return rowOrder;
    }

    public void setRowOrder(Integer rowOrder) {
        this.rowOrder = rowOrder;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
