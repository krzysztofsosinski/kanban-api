package pl.ks.board.domain.board;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.ks.board.infrastructure.exception.ResourceNotFoundException;
import pl.ks.board.infrastructure.util.ServiceUtil;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CardService {

    private final CardRepository cardRepository;

    @Autowired
    public CardService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    @Transactional(readOnly = true)
    public List<Card> retrieveAll() {
        return cardRepository.findAllCards();
    }

    @Transactional(readOnly = true)
    public Card retrieveCard(final Long id) {
        return cardRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Could not find card with id = " + id));
    }

    @Transactional
    public Card saveCard(final Card card) {
        return cardRepository.save(card);
    }

    @Transactional
    public Card patchCard(final Map<String, Object> fields, final Long cardId) {
        Optional<Card> card = cardRepository.findById(cardId);

        if (card.isPresent()) {
            return cardRepository.save(ServiceUtil.patchObject(fields, card.get(), Card.class));
        }

        throw new ResourceNotFoundException("Could not find card with id = " + cardId);
    }

    @Transactional
    public Card updateCard(JSONObject cardJSON, Long cardId) throws JSONException {
        Optional<Card> cardOpt = cardRepository.findById(cardId);
        Card card = cardOpt.orElse(null);
        if (card != null) {
            card.setStatus(cardJSON.getString("status"));
            card.setRowOrder(cardJSON.getInt("rowOrder"));
            card.setUpdatedAt(new Date());
            return cardRepository.save(card);
        } else {
            throw new ResourceNotFoundException("Could not find card with id = " + cardId);
        }
    }

    @Transactional
    public void deleteCard(Long id) {
        cardRepository.deleteById(id);
    }
}
