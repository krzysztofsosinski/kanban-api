package pl.ks.board.boundry.model.kanban;

import pl.ks.board.domain.board.Task;

import java.io.Serializable;

public class TaskDto implements Serializable {

    private Long id;
    private String name;
    private Boolean done;

    public Task asTask() {
        return new Task(id, name, done);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
}
