package pl.ks.board.boundry.web;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.ks.board.boundry.model.kanban.CardDto;
import pl.ks.board.boundry.model.kanban.TaskDto;
import pl.ks.board.domain.board.Card;
import pl.ks.board.domain.board.CardService;
import pl.ks.board.domain.board.TaskService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cards")
@PreAuthorize("hasRole('USER')")
public class CardController {

    private final CardService cardService;
    private final TaskService taskService;
    private final ModelMapper modelMapper;

    @Autowired
    public CardController(CardService cardService, TaskService taskService,
                          ModelMapper modelMapper) {
        this.cardService = cardService;
        this.taskService = taskService;
        this.modelMapper = modelMapper;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<CardDto> findAll() {
        return cardService.retrieveAll()
                .stream()
                .map(card -> convert(card, CardDto.class))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public CardDto findCardById(@PathVariable(value = "id") Long id) {
        return convert(cardService.retrieveCard(id), CardDto.class);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CardDto updateCard(@RequestBody Map<String, Object> payload, @PathVariable("id") Long cardId) {
        return convert(cardService.patchCard(payload, cardId), CardDto.class);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public CardDto saveCard(@RequestBody CardDto cardDto) {
        return convert(cardService.saveCard(convert(cardDto, Card.class)), CardDto.class);
    }

    @PostMapping(value = "/{cardId}/tasks")
    @ResponseStatus(HttpStatus.CREATED)
    public TaskDto saveTask(@PathVariable("cardId") Long cardId, @RequestBody TaskDto taskDTO) {
        return convert(taskService.createTask(cardId, taskDTO), TaskDto.class);
    }

    @PutMapping(value = "/{cardId}/tasks/{taskId}")
    @ResponseStatus(HttpStatus.OK)
    public TaskDto updateTask(@PathVariable("cardId") Long cardId,
                              @PathVariable("taskId") Long taskId,
                              @RequestBody Map<String, Object> payload) {
        return convert(taskService.patchTask(taskId, payload), TaskDto.class);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCard(@PathVariable("id") Long id) {
        cardService.deleteCard(id);
    }

    @DeleteMapping(value = "/{cardId}/tasks/{taskId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTask(@PathVariable("cardId") Long cardId, @PathVariable("taskId") Long taskId) {
        taskService.deleteTask(taskId);
    }

    private <V, T> T convert(V objToConvert, Class<T> clazz) {
        return modelMapper.map(objToConvert, clazz);
    }

}