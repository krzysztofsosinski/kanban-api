package pl.ks.board.infrastructure.mailing;

public interface EmailSender {

    void sendEmail(String to, String subject, String content);

}
