package pl.ks.board.infrastructure.util;

import org.springframework.util.ReflectionUtils;

import java.util.Map;
import java.util.Optional;

public class ServiceUtil {

    public static <T> T patchObject(final Map<String, Object> fields, final T objectToPatch, final Class<T> type) {
        for (Map.Entry<String, Object> field : fields.entrySet()) {
            Optional.ofNullable(ReflectionUtils.findField(type, field.getKey()))
                    .ifPresent((f) -> {
                        ReflectionUtils.makeAccessible(f);
                        ReflectionUtils.setField(f, objectToPatch, field.getValue());
                    });
        }

        return objectToPatch;
    }

}
