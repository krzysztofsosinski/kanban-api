package pl.ks.board.infrastructure.type;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.java.JavaTypeDescriptor;
import org.hibernate.type.descriptor.sql.SqlTypeDescriptor;
import org.hibernate.usertype.DynamicParameterizedType;

import java.util.Properties;

public class JSONBinaryType extends AbstractSingleColumnStandardBasicType<Object> implements DynamicParameterizedType {

    public JSONBinaryType(SqlTypeDescriptor sqlTypeDescriptor, JavaTypeDescriptor<Object> javaTypeDescriptor) {
        super(sqlTypeDescriptor, javaTypeDescriptor);
    }

    @Override
    public String getName() {
        return "jsonb";
    }

    @Override
    public void setParameterValues(Properties properties) {

    }
}
