package pl.ks.user.boundry.web.filter;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.ks.user.domain.token.JwtTokenService;
import pl.ks.user.infrastructure.security.TokenBasedAuth;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TokenAuthFilter extends OncePerRequestFilter {

    private JwtTokenService jwtTokenService;

    private UserDetailsService userDetailsService;

    public TokenAuthFilter(JwtTokenService tokenHelper, UserDetailsService userDetailsService) {
        this.jwtTokenService = tokenHelper;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String username;
        String authToken = jwtTokenService.getToken(request);
        if (authToken != null) {
            // get username from token
            username = jwtTokenService.getUsernameFromToken(authToken);
            if (username != null) {
                // get user
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                if (jwtTokenService.validateToken(authToken, userDetails)) {
                    // create authentication
                    TokenBasedAuth authentication = new TokenBasedAuth(userDetails);
                    authentication.setToken(authToken);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }

        chain.doFilter(request, response);
    }
}
