package pl.ks.user.boundry.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FullJwt {

    private String accessToken;
    private String refreshToken;
    private int expiresIn;

}
