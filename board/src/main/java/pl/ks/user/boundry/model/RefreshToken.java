package pl.ks.user.boundry.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class RefreshToken {

    @NotBlank
    @JsonProperty
    private final String refreshToken;

    @JsonCreator
    public RefreshToken(@NotBlank String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}
