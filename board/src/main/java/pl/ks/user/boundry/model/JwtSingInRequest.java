package pl.ks.user.boundry.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtSingInRequest {

    private String firstName;
    private String lastName;
    private String email;
    private String password;

}
