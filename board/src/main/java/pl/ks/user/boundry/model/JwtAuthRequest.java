package pl.ks.user.boundry.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtAuthRequest {

    private String username;
    private String password;

    public JwtAuthRequest() {
    }

    public JwtAuthRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
