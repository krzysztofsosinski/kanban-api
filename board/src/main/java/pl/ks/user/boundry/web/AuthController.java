package pl.ks.user.boundry.web;

import com.google.common.collect.ImmutableMap;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import pl.ks.user.boundry.model.*;
import pl.ks.user.domain.token.JwtTokenService;
import pl.ks.user.domain.user.KanbanUserDetailsService;
import pl.ks.user.domain.user.User;
import pl.ks.user.infrastructure.exception.DuplicateEmailException;
import pl.ks.user.infrastructure.exception.InvalidRefreshTokenException;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {

    private final JwtTokenService jwtTokenService;
    private final AuthenticationManager authenticationManager;
    private final KanbanUserDetailsService userDetailsService;

    public AuthController(JwtTokenService jwtTokenService, AuthenticationManager authenticationManager,
                          KanbanUserDetailsService userDetailsService) {
        this.jwtTokenService = jwtTokenService;
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/signup")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    FullJwt createAccount(@RequestBody JwtSingInRequest jwtSingInRequest) {
        if (userDetailsService.isExist(jwtSingInRequest.getEmail())) {
            throw new DuplicateEmailException();
        }

        User user = userDetailsService.registerUser(jwtSingInRequest);

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        user.getUsername(),
                        jwtSingInRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtTokenService.generateToken(user.getUsername());
        String refreshToken = jwtTokenService.createRefreshToken(user);
        int expiresIn = jwtTokenService.getExpiredIn();

        return new FullJwt(jwt, refreshToken, expiresIn);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/singin")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    FullJwt createAuthToken(@RequestBody JwtAuthRequest jwtAuthRequest) {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        jwtAuthRequest.getUsername(),
                        jwtAuthRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        User user = (User) authentication.getPrincipal();
        String jwt = jwtTokenService.generateToken(user.getUsername());
        String refreshToken = jwtTokenService.createRefreshToken(user);
        int expiresIn = jwtTokenService.getExpiredIn();

        return new FullJwt(jwt, refreshToken, expiresIn);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/refresh")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    AccessToken refreshToken(@Valid @RequestBody RefreshToken refreshToken) {
        return jwtTokenService.refreshAccessToken(refreshToken.getRefreshToken())
                .orElseThrow(InvalidRefreshTokenException::new);

    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/logout")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void logout(@Valid @RequestBody RefreshToken refreshToken) {
        jwtTokenService.logoutUser(refreshToken.getRefreshToken());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/change-password")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<Map<String, String>> changePassword(@RequestBody PasswordChanger passwordChanger) {
        userDetailsService.changePassword(passwordChanger.oldPassword, passwordChanger.newPassword);
        return ResponseEntity.accepted().body(ImmutableMap.of("result", "success"));
    }

    static class PasswordChanger {
        String oldPassword;
        String newPassword;
    }

}
