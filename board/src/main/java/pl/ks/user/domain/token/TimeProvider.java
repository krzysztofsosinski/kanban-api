package pl.ks.user.domain.token;

import java.util.Date;

public interface TimeProvider {

    Date now();

}
