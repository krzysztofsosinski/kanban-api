package pl.ks.user.domain.token;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRefreshTokenRepository extends CrudRepository<UserRefreshToken, Long> {

    Optional<UserRefreshToken> findByToken(String token);

}
