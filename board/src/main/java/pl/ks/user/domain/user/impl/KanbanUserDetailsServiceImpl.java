package pl.ks.user.domain.user.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.ks.user.boundry.model.JwtSingInRequest;
import pl.ks.user.domain.user.*;
import pl.ks.user.infrastructure.exception.DuplicateEmailException;
import pl.ks.user.infrastructure.exception.RoleNotFoundException;
import pl.ks.user.infrastructure.exception.UserNotFoundException;

import java.util.Optional;

@Service("KanbanUserDetailsService")
public class KanbanUserDetailsServiceImpl implements KanbanUserDetailsService {

    private final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
        String username = currentUser.getName();

        if (authenticationManager != null) {
            LOGGER.debug("Re-authenticating user '" + username + "' for password change request.");
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, oldPassword));
        } else {
            LOGGER.debug("No authentication manager set. can't change Password!");
            return;
        }

        LOGGER.debug("Changing password for user '" + username + "'");

        User user = (User) loadUserByUsername(username);

        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);
    }

    @Override
    public User registerUser(JwtSingInRequest jwtSingInRequest) {
        Optional<User> existingUser = userRepository.findByUsername(jwtSingInRequest.getEmail());
        if (existingUser.isPresent()) {
            throw new DuplicateEmailException();
        }

        User user = new User(
                jwtSingInRequest.getEmail(),
                passwordEncoder.encode(jwtSingInRequest.getPassword()),
                jwtSingInRequest.getFirstName(),
                jwtSingInRequest.getLastName()
        );

        user.addAuthority(authorityRepository.findByName(Authority.RoleEnum.ROLE_USER).orElseThrow(RoleNotFoundException::new));

        return userRepository.save(user);
    }

    @Override
    public boolean isExist(String userName) {
        return userRepository.existsByUsername(userName);
    }
}
