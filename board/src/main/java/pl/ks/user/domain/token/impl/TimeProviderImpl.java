package pl.ks.user.domain.token.impl;

import org.springframework.stereotype.Component;
import pl.ks.user.domain.token.TimeProvider;

import java.time.Instant;
import java.util.Date;

@Component
public class TimeProviderImpl implements TimeProvider {

    @Override
    public Date now() {
        return Date.from(Instant.now());
    }

}
