package pl.ks.user.domain.user;

import org.springframework.security.core.userdetails.UserDetailsService;
import pl.ks.user.boundry.model.JwtSingInRequest;

public interface KanbanUserDetailsService extends UserDetailsService {

    void changePassword(String oldPassword, String newPassword);

    User registerUser(JwtSingInRequest jwtSingInRequest);

    boolean isExist(String userName);

}
