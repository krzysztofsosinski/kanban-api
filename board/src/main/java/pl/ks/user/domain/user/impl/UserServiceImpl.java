package pl.ks.user.domain.user.impl;

import org.springframework.stereotype.Service;
import pl.ks.user.domain.user.User;
import pl.ks.user.domain.user.UserService;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Override
    public User findById(Long id) {
        return null;
    }

    @Override
    public User findByUsername(String username) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }
}
