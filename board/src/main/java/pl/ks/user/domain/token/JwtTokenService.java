package pl.ks.user.domain.token;

import org.springframework.security.core.userdetails.UserDetails;
import pl.ks.user.boundry.model.AccessToken;
import pl.ks.user.domain.user.User;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Optional;

public interface JwtTokenService {

    String getUsernameFromToken(String token);

    Date getIssuedAtDateFromToken(String token);

    String generateToken(String username);

    int getExpiredIn();

    Boolean validateToken(String token, UserDetails userDetails);

    String createRefreshToken(User user);

    Optional<AccessToken> refreshAccessToken(String refreshToken);

    String getToken(HttpServletRequest request);

    void logoutUser(String refreshToken);

    String getAuthHeaderFromHeader(HttpServletRequest request);
}
