package pl.ks.user.domain.token;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.ks.user.domain.user.User;

import javax.persistence.*;

@Entity
@Table(name = "USER_REFRESH_TOKEN")
public class UserRefreshToken {

    @Id
    @SequenceGenerator(name = "user_refresh_token_id_seq", sequenceName = "user_refresh_token_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_refresh_token_id_seq")
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(nullable = false)
    private String token;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    protected UserRefreshToken() {
    }

    public UserRefreshToken(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
