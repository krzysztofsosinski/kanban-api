CREATE SEQUENCE IF NOT EXISTS user_refresh_token_id_seq
    START 1
    INCREMENT 1
    MINVALUE 1;

CREATE TABLE IF NOT EXISTS public.user_refresh_token
(
    id      INTEGER PRIMARY KEY DEFAULT nextval('user_refresh_token_id_seq'::regclass),
    token   VARCHAR(128)                  NOT NULL,
    user_id INTEGER REFERENCES users (id) NOT NULL
);