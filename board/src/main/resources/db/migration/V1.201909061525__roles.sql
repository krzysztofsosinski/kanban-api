INSERT INTO authority(id, name)
VALUES (nextval('authority_id_seq'),
        'ROLE_USER'),
       (nextval('authority_id_seq'),
        'ROLE_ADMIN');