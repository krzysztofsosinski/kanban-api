CREATE SEQUENCE IF NOT EXISTS user_id_seq
  START 1
  INCREMENT 1
  MINVALUE 1;

CREATE TABLE IF NOT EXISTS public.users
(
  id                       INTEGER PRIMARY KEY DEFAULT nextval('user_id_seq'::regclass),
  username                 VARCHAR(255) not null,
  password                 VARCHAR(255) not null,
  first_name               VARCHAR(255),
  last_name                VARCHAR(255),
  last_password_reset_date TIMESTAMP
);

CREATE SEQUENCE IF NOT EXISTS authority_id_seq
  START 1
  INCREMENT 1
  MINVALUE 1;

CREATE TABLE IF NOT EXISTS public.authority
(
  id   INTEGER PRIMARY KEY DEFAULT nextval('authority_id_seq'::regclass),
  name VARCHAR(128) NOT NULL
);

CREATE TABLE IF NOT EXISTS public.user_authority
(
  user_id      INTEGER REFERENCES users (id),
  authority_id INTEGER REFERENCES authority (id)
);

CREATE SEQUENCE IF NOT EXISTS card_id_seq
  START 1
  INCREMENT 1
  MINVALUE 1;

CREATE TABLE IF NOT EXISTS public.cards
(
  id          INTEGER PRIMARY KEY DEFAULT nextval('card_id_seq'::regclass),
  title       VARCHAR(521) NOT NULL,
  description TEXT,
  color       VARCHAR(10),
  status      VARCHAR(128),
  create_at   DATE         NOT NULL,
  updated_at  DATE,
  row_order   INTEGER
);

CREATE SEQUENCE IF NOT EXISTS task_id_seq
  START 1
  INCREMENT 1
  MINVALUE 1;

CREATE TABLE IF NOT EXISTS public.tasks
(
  id      INTEGER PRIMARY KEY DEFAULT nextval('task_id_seq'::regclass),
  name    VARCHAR(512) NOT NULL,
  done    BOOLEAN             default false,
  card_id BIGINT REFERENCES cards (id)
);