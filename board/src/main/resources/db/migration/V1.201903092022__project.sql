CREATE SEQUENCE IF NOT EXISTS project_id_seq
    START 1
    INCREMENT 1
    MINVALUE 1;

CREATE TABLE IF NOT EXISTS public.projects
(
    id          INTEGER PRIMARY KEY DEFAULT nextval('project_id_seq'::regclass),
    name        VARCHAR(255)  not null,
    description VARCHAR(2048) not null
);

CREATE TABLE IF NOT EXISTS public.project_user
(
    user_id    INTEGER REFERENCES users (id),
    project_id INTEGER REFERENCES projects (id)
);

CREATE SEQUENCE IF NOT EXISTS board_id_seq
    START 1
    INCREMENT 1
    MINVALUE 1;

CREATE TABLE IF NOT EXISTS public.boards
(
    id          INTEGER PRIMARY KEY DEFAULT nextval('board_id_seq'::regclass),
    name        VARCHAR(255) not null,
    description VARCHAR(1024),
    project_id  INTEGER REFERENCES projects (id),
    position    SMALLINT            DEFAULT 0
);